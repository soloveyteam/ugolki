﻿using System;
using UnityEngine;

public class BoardManager : MonoBehaviour
{
    private static BoardManager m_instance;
    private BoardBuilder boardBuilder;
    private PawnBuilder pawnBuilder;
    private GameObject[][] cells;

    public GameObject[][] Cells
    {
        get { return cells; }
        set { cells = value; }
    }

    public static BoardManager Instance
    {
        get
        {
            if (m_instance == null)
            {
                var controller = Instantiate(Resources.Load("Prefabs/BoardManager")) as GameObject;
                m_instance = controller.GetComponent<BoardManager>();
            }
            return m_instance;
        }
    }

    public BoardBuilder BoardBuilder
    {
        get 
        {
            if (boardBuilder)
            {
                return boardBuilder;
            }
            else
            {
                throw new NullReferenceException("");
            }
        }
    }

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (m_instance != this) Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    private void Start()
    {
        MoveManager.Instance.WinnerIsDeterminedHandler += DestroyCurrentLevel;
    }

    private void BuildBoard()
    {
        boardBuilder = Instantiate(Resources.Load<BoardBuilder>("Prefabs/BoardBuilder"));
    }

    private void BuildPawns()
    {
        pawnBuilder = Instantiate(Resources.Load<PawnBuilder>("Prefabs/PawnBuilder"));
    }

    public void CreateNewLevel()
    {
        BuildBoard();
        BuildPawns();
    }

    private void DestroyCurrentLevel(Winner winner)
    {
        Destroy(GameObject.FindGameObjectWithTag("BoardBuilder"));
        Destroy(GameObject.FindGameObjectWithTag("PawnBuilder"));
        DestroyCells();
        DestroyPawns();
        DestroyEdging();
    }

    private void DestroyCells()
    {
        GameObject[] cells = GameObject.FindGameObjectsWithTag("Cell");
        foreach(var cell in cells)
        {
            Destroy(cell);
        }
        Destroy(GameObject.FindGameObjectWithTag("CellsParent"));
    }

    private void DestroyPawns()
    {
        GameObject[] pawns = GameObject.FindGameObjectsWithTag("Pawn");
        foreach (var pawn in pawns)
        {
            Destroy(pawn);
        }
    }

    private void DestroyEdging()
    {
        GameObject[] edgCells = GameObject.FindGameObjectsWithTag("EdgCell");
        foreach (var edgCell in edgCells)
        {
            Destroy(edgCell);
        }
        Destroy(GameObject.FindGameObjectWithTag("EdgingParent"));
    }

    private void OnDestroy()
    {
        MoveManager.Instance.WinnerIsDeterminedHandler -= DestroyCurrentLevel;
    }
}
