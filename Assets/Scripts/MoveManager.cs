﻿using System.Collections.Generic;
using UnityEngine;

public delegate void MoveIsDone(bool isWhite);
public delegate void WinnerIsDetermined(Winner winner);

public class MoveManager : MonoBehaviour
{
    private static MoveManager m_instance;
    private GameObject selectedPawn;
    private List<GameObject> possibleMovies;
    public event MoveIsDone MoveIsDoneHandler;
    public event WinnerIsDetermined WinnerIsDeterminedHandler;
    public VariantOfJumps CurrentJumps = VariantOfJumps.WithoutJumps;

    public static MoveManager Instance
    {
        get
        {
            if (m_instance == null)
            {
                var controller = Instantiate(Resources.Load("Prefabs/MoveManager")) as GameObject;
                m_instance = controller.GetComponent<MoveManager>();
            }
            return m_instance;
        }
    }

    private void Awake()
    {
        if (m_instance == null)
        {
            m_instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            if (m_instance != this) Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    /// <summary>
    /// Рассчитать возможные ходы для выделенной пешки.
    /// </summary>
    private void CalculatePossibleMoves()
    {
        GameObject[][] cells = BoardManager.Instance.Cells;

        int x, y;

        GetCurrentCellPositions(cells, out x, out y);

        // Если вернулись положительные значения, то ячейка найдена.
        if (x >=0 && y >= 0)
        {
            // Добавляем возможные ходы без прыжков.
            possibleMovies = GetStandardPossibleMoves(cells, x, y);

            // Если есть условие на прыжки по диагонали, то добавляем их.
            if (CurrentJumps == VariantOfJumps.DiagonalJumps)
            {
                possibleMovies.AddRange(GetDiagonalJumpsPossibleMoves(cells, x, y));
            }

            // Если есть условие на прыжки по горизонтали и вертикали, то добавляем их.
            if (CurrentJumps == VariantOfJumps.HorizontalJumps)
            {
                possibleMovies.AddRange(GetHorizontalAndVerticalJumpsPossibleMoves(cells, x, y));
            }

            if (possibleMovies.Count != 0)
            {
                // Подсветить возможные ходы.
                HighlightPossibleMoves(possibleMovies);
            }
        }
    }

    /// <summary>
    /// Получить позицию выделенной пешки в двумерном массиве.
    /// </summary>
    /// <param name="cells"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    private void GetCurrentCellPositions(GameObject[][] cells, out int x, out int y)
    {
        GameObject curCell = selectedPawn.transform.parent.gameObject.transform.parent.gameObject;
        for (int i = 0; i < cells.Length; i++)
        {
            for (int j = 0; j < cells.Length; j++)
            {
                if (cells[i][j].Equals(curCell))
                {
                    x = i;
                    y = j;
                    return;
                }
            }
        }
        x = -1;
        y = -1;
    }

    /// <summary>
    /// Получить коллекцию возможных ходов.
    /// </summary>
    /// <param name="cells"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private List<GameObject> GetStandardPossibleMoves(GameObject[][] cells, int x, int y)
    {
        List<GameObject> result = new List<GameObject>();

        int max_x = cells.Length - 1;
        int max_y = max_x;
        for (int dx = (x > 0 ? -1 : 0); dx <= (x < max_x ? 1 : 0); ++dx)
        {
            for (int dy = (y > 0 ? -1 : 0); dy <= (y < max_y ? 1 : 0); ++dy)
            {
                if (dx != 0 || dy != 0)
                {
                    Cell cell = cells[x + dx][y + dy].GetComponent<Cell>();
                    // И целлс не имеет потомка
                    if (!cell.IsHaveAPawn)
                    {
                        result.Add(cells[x + dx][y + dy]);
                    }
                }
            }
        }
        return result;
    }

    /// <summary>
    /// Получить коллекцию возможных прыжков по диагонали.
    /// </summary>
    /// <param name="cells"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private List<GameObject> GetDiagonalJumpsPossibleMoves(GameObject[][] cells, int x, int y)
    {
        List<GameObject> result = new List<GameObject>();

        int max_x = cells.Length - 1;
        int max_y = max_x;

        // Левая нижняя.
        if (x > 1 && 
            y > 1 && 
            cells[x - 1][y - 1].GetComponent<Cell>().IsHaveAPawn && 
            !cells[x - 2][y - 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x - 2][y - 2]);
        }

        // Левая верхня.
        if (x > 1 && 
            y < max_y - 1 && 
            cells[x - 1][y + 1].GetComponent<Cell>().IsHaveAPawn && 
            !cells[x - 2][y + 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x - 2][y + 2]);
        }

        // Правая верхняя.
        if (x < max_x - 1 && 
            y < max_y - 1 && 
            cells[x + 1][y + 1].GetComponent<Cell>().IsHaveAPawn && 
            !cells[x + 2][y + 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x + 2][y + 2]);
        }

        // Правая нижняя.
        if (x < max_x - 1 && 
            y > 1 && 
            cells[x + 1][y - 1].GetComponent<Cell>().IsHaveAPawn && 
            !cells[x + 2][y - 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x + 2][y - 2]);
        }

        return result;
    }

    /// <summary>
    /// Получить коллекцию возможных прыжков по горизонтали и вертикали.
    /// </summary>
    /// <param name="cells"></param>
    /// <param name="x"></param>
    /// <param name="y"></param>
    /// <returns></returns>
    private List<GameObject> GetHorizontalAndVerticalJumpsPossibleMoves(GameObject[][] cells, int x, int y)
    {
        List<GameObject> result = new List<GameObject>();

        int max_x = cells.Length - 1;
        int max_y = max_x;

        // Нижняя.
        if (y > 1 &&
            cells[x][y - 1].GetComponent<Cell>().IsHaveAPawn &&
            !cells[x][y - 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x][y - 2]);
        }

        // Левая.
        if (x > 1 &&
            cells[x - 1][y].GetComponent<Cell>().IsHaveAPawn &&
            !cells[x - 2][y].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x - 2][y]);
        }

        // Верхняя.
        if (y < max_y - 1 &&
            cells[x][y + 1].GetComponent<Cell>().IsHaveAPawn &&
            !cells[x][y + 2].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x][y + 2]);
        }

        // Правая.
        if (x < max_x - 1 &&
            cells[x + 1][y].GetComponent<Cell>().IsHaveAPawn &&
            !cells[x + 2][y].GetComponent<Cell>().IsHaveAPawn)
        {
            result.Add(cells[x + 2][y]);
        }

        return result;
    }

    /// <summary>
    /// Подсветить возможные ходы.
    /// </summary>
    /// <param name="possibleMoviesCells"></param>
    private void HighlightPossibleMoves(List<GameObject> possibleMoviesCells)
    {
        for (int i = 0; i < possibleMoviesCells.Count; i++)
        {
            possibleMoviesCells[i].GetComponent<Cell>().HighlightPossibleMovePlace(true);
        }
    }

    /// <summary>
    /// Очистить коллекцию с возможными ходами.
    /// </summary>
    private void ClearPossibleMoves()
    {
        foreach (var e in possibleMovies)
        {
            e.GetComponent<Cell>().HighlightPossibleMovePlace(false);
        }
        possibleMovies.Clear();
    }

    /// <summary>
    /// Обработчик клика игроком на пешку.
    /// </summary>
    /// <param name="pawn"></param>
    public void PawnClickHandler(Pawn pawn)
    {
        if (pawn != null && selectedPawn == null)
        {
            selectedPawn = pawn.gameObject;
            CalculatePossibleMoves();
        }
        else if (pawn != null && pawn.gameObject != selectedPawn)
        {
            selectedPawn.GetComponent<Pawn>().SetSelect(false);
            ClearPossibleMoves();
            selectedPawn = pawn.gameObject; 
            CalculatePossibleMoves();
        }
        else if (pawn != null && pawn.gameObject == selectedPawn)
        {
            selectedPawn = null;
            ClearPossibleMoves();
        }
    }

    /// <summary>
    /// Обработчик клика игроком на клетку шахматной доски.
    /// </summary>
    /// <param name="cell"></param>
    public void CellClickHandler(Cell cell)
    {
        if (selectedPawn && possibleMovies != null && possibleMovies.Contains(cell.gameObject))
        {
            Move move = new Move(selectedPawn, cell.gameObject);
            MoveMaker.MakeMove(move);
            SetPlayersTurn(selectedPawn.GetComponent<Pawn>());
            Winner winner = EndGameChecker.DetermineWinner(BoardManager.Instance.Cells, selectedPawn.GetComponent<Pawn>().IsWhite);

            if (winner != Winner.None)
                WinnerIsDeterminedHandler?.Invoke(winner);

            selectedPawn.GetComponent<Pawn>().IsClicked = false;
        }
    }

    /// <summary>
    /// Установить чья очередь ходить. В соответствии с переданным параметром пешки меняют свою интерактивность.
    /// </summary>
    /// <param name="pawn"></param>
    private void SetPlayersTurn(Pawn pawn)
    {
        MoveIsDoneHandler(pawn.IsWhite);
    }
}

public enum VariantOfJumps
{
    DiagonalJumps,
    HorizontalJumps,
    WithoutJumps
}
