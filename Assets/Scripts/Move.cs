﻿using UnityEngine;

public class Move
{
    public GameObject pawn;
    public GameObject perCell;

    public Move(GameObject Pawn, GameObject PerCell)
    {
        pawn = Pawn;
        perCell = PerCell;
    }
}
