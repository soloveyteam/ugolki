﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public delegate void CellWasClicked(Cell cell);
public class Cell : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Sprite blackSprite;
    [SerializeField] private Sprite whiteSprite;
    [SerializeField] private Transform pawnPlace;
    [SerializeField] private GameObject possibleMove;

    private HomeForPawnOfColor homeForPawnOfColor = HomeForPawnOfColor.IsNotHome;
    private bool isWhite;
    public event CellWasClicked CellClickHandler;

    public HomeForPawnOfColor HomeForPawnOfColor
    {
        get { return homeForPawnOfColor; }
        set { homeForPawnOfColor = value; }
    }

    public bool IsHaveAPawn
    {
        get;
        private set;
    }

    public bool IsFree { get; }

    public bool IsWhite
    {
        get { return isWhite; }
        set
        {
            isWhite = value;
            if (isWhite)
            {
                GetComponent<Image>().sprite = whiteSprite;
            }
            else
            {
                GetComponent<Image>().sprite = blackSprite;
            }
            
        }
    }

    private void Start()
    {
        CellClickHandler += MoveManager.Instance.CellClickHandler;
    }

    /// <summary>
    /// Прикрепляет к себе пешку.
    /// </summary>
    /// <param name="pawn"></param>
    public void AttachPawn(Transform pawn)
    {
        pawn.SetParent(pawnPlace);
        pawn.localPosition = Vector3.zero;
        IsHaveAPawn = true;
    }

    /// <summary>
    /// Возвращает Transform прикреплённой пешки.
    /// </summary>
    /// <returns></returns>
    public Transform GetAttachedPawn()
    {
        if (IsHaveAPawn)
        {
            IsHaveAPawn = false;
            return pawnPlace.GetChild(0);
        }
        else
        {
            return null;
        }
    }

    /// <summary>
    /// Активирует визуализацию возможного хода на себя.
    /// </summary>
    /// <param name="isActivate"></param>
    public void HighlightPossibleMovePlace (bool isActivate)
    {
        possibleMove.SetActive(isActivate);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        CellClickHandler?.Invoke(this);
    }

    private void OnDestroy()
    {
        CellClickHandler -= MoveManager.Instance.CellClickHandler;
    }
}

public enum HomeForPawnOfColor
{
    IsNotHome,
    White,
    Black
}
