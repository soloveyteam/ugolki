﻿using UnityEngine;

public class PawnBuilder : MonoBehaviour
{
    [SerializeField] private GameObject pawnPrefab;


    void Start()
    {
        GameObject[][] cells = BoardManager.Instance.BoardBuilder.Cells;
        CreatePawns(cells);
    }

    /// <summary>
    /// Создаёт пешки в сцене.
    /// </summary>
    /// <param name="cells"></param>
    private void CreatePawns(GameObject[][] cells)
    {
        int boardWidth = cells.Length;

        for (int i = 0; i < boardWidth; i++)
        {
            for (int j = 0; j < boardWidth; j++)
            {
                Cell cellScript = cells[i][j].GetComponent<Cell>();

                if (cellScript)
                {
                    if (cellScript.HomeForPawnOfColor != HomeForPawnOfColor.IsNotHome)
                    {
                        CreatePawn(cellScript);
                    }
                }
            }
        }
    }

    /// <summary>
    /// Создаёт пешку в сцене, определяет её цвет и прикрепляет её к переданной клетке.
    /// </summary>
    /// <param name="cellParent"></param>
    private void CreatePawn(Cell cellParent)
    {
        Pawn pawnScript = Instantiate(pawnPrefab).GetComponent<Pawn>();
        if (cellParent.HomeForPawnOfColor == HomeForPawnOfColor.White)
        {
            pawnScript.IsWhite = true;
        }
        else
        {
            pawnScript.IsWhite = false;
        }
        cellParent.AttachPawn(pawnScript.transform);
    }
}
