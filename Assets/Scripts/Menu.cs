﻿using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
    [SerializeField] private CanvasGroup StartWindow;
    [SerializeField] private CanvasGroup EndGameWindow;
    [SerializeField] private Text _winner;

    private void Start()
    {
        MoveManager.Instance.WinnerIsDeterminedHandler += EndGame;
    }

    private void ShowWindow(CanvasGroup window)
    {
        window.alpha = 1f;
        window.blocksRaycasts = true;
        window.interactable = true;
    }

    private void HideWindow(CanvasGroup window)
    {
        window.alpha = 0f;
        window.blocksRaycasts = false;
        window.interactable = false;
    }

    public void SetMovesWithDiagonalJumps()
    {
        StartGame(VariantOfJumps.DiagonalJumps);
    }

    public void SetMovesWithHorizontalAndVerticalJumps()
    {
        StartGame(VariantOfJumps.HorizontalJumps);
    }

    public void SetMovesWithoutJumps()
    {
        StartGame(VariantOfJumps.WithoutJumps);
    }

    private void StartGame(VariantOfJumps currentJumps)
    {
        MoveManager.Instance.CurrentJumps = currentJumps;
        BoardManager.Instance.CreateNewLevel();
        HideWindow(StartWindow);
    }

    private void EndGame(Winner winner)
    {
        if (winner == Winner.White)
        {
            _winner.text = "белых";
        }
        else if (winner == Winner.Black)
        {
            _winner.text = "чёрных";
        }
        ShowWindow(EndGameWindow);
    }

    public void NewGame()
    {
        HideWindow(EndGameWindow);
        ShowWindow(StartWindow);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    private void OnDestroy()
    {
        MoveManager.Instance.WinnerIsDeterminedHandler -= EndGame;
    }
}
