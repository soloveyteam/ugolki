﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public delegate void PawnWasClicked(Pawn pawn);

public class Pawn : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private Sprite blackSprite;
    [SerializeField] private Sprite whiteSprite;
    private bool isWhite;
    private bool isClicked;
    public event PawnWasClicked PawnClickHandler;

    public bool IsClicked
    {
        get { return isClicked; }
        set
        {
            if (!isClicked)
            {
                isClicked = !isClicked;
                SetSelect(isClicked);
                PawnClickHandler?.Invoke(this);  
            }
            else
            {
                isClicked = !isClicked;
                SetSelect(isClicked);
                PawnClickHandler?.Invoke(this);
            }
        }
    }

    public bool IsWhite
    {
        get { return isWhite; }
        set
        {
            isWhite = value;
            if (isWhite)
            {
                GetComponent<Image>().sprite = whiteSprite;
            }
            else
            {
                GetComponent<Image>().sprite = blackSprite;
            }
        }
    }

    /// <summary>
    /// Изменяет цвет пешки, если та выделена.
    /// </summary>
    /// <param name="isSelected"></param>
    public void SetSelect(bool isSelected)
    {
        if (isSelected)
        {
            GetComponent<Image>().color = Color.yellow;
        }
        else
        {
            GetComponent<Image>().color = Color.white;
            isClicked = false;
        }
    }

    private void Start()
    {
        PawnClickHandler += MoveManager.Instance.PawnClickHandler;
        MoveManager.Instance.MoveIsDoneHandler += EnableIneractivity;
        EnableIneractivity(false);
    }
    
    /// <summary>
    /// Изменяет возможность взаимодействия игрока с пешкой.
    /// </summary>
    /// <param name="isWhite"></param>
    private void EnableIneractivity(bool isWhite)
    {
        if (isWhite == this.isWhite)
        {
            gameObject.GetComponent<Image>().raycastTarget = false;
        }
        else
        {
            gameObject.GetComponent<Image>().raycastTarget = true;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        IsClicked = !IsClicked;
        
    }

    private void OnDestroy()
    {
        PawnClickHandler -= MoveManager.Instance.PawnClickHandler;
        MoveManager.Instance.MoveIsDoneHandler -= EnableIneractivity;
    }
}
