﻿using System.Collections.Generic;
using UnityEngine;

public static class EndGameChecker
{
    public static Winner DetermineWinner(GameObject[][] board, bool lastMoveIsWhite)
    {
        GetHouses(board, out List<Cell> blackHouses, out List<Cell> whiteHouses);

        if (lastMoveIsWhite)
        {
            bool whiteWin = true;
            foreach (var blackHouse in blackHouses)
            {
                whiteWin = whiteWin &&
                            blackHouse.IsHaveAPawn &&
                            blackHouse.transform.GetChild(0).GetComponentInChildren<Pawn>().IsWhite;
            }

            if (whiteWin)
            {
                return Winner.White;
            }
        }
        else
        {
            bool blackWin = true;
            foreach (var whiteHouse in whiteHouses)
            {
                blackWin = blackWin &&
                            whiteHouse.IsHaveAPawn &&
                            !whiteHouse.transform.GetChild(0).GetComponentInChildren<Pawn>().IsWhite;
            }

            if (blackWin)
            {
                return Winner.Black;
            }
        }
        
        return Winner.None;
    }

    private static void GetHouses(GameObject[][] board, out List<Cell> blackHouses, out List<Cell> whiteHouses)
    {
        blackHouses = new List<Cell>();
        whiteHouses = new List<Cell>();

        for (int x = 0; x < board.Length; x++)
        {
            for (int y = 0; y < board.Length; y++)
            {
                Cell cell = board[x][y].GetComponent<Cell>();

                if (cell.HomeForPawnOfColor == HomeForPawnOfColor.Black)
                {
                    blackHouses.Add(cell);
                }

                if (cell.HomeForPawnOfColor == HomeForPawnOfColor.White)
                {
                    whiteHouses.Add(cell);
                }
            }
        }
    }
}

public enum Winner
{
    None,
    Black,
    White
}
