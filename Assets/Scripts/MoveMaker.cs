﻿public static class MoveMaker
{
    public static void MakeMove(Move move)
    {
        Cell _perCell = move.perCell.GetComponent<Cell>();
        Cell _fromCell = move.pawn.transform.parent.gameObject.transform.parent.gameObject.GetComponent<Cell>();

        _perCell.AttachPawn(_fromCell.GetAttachedPawn());
    }
}
