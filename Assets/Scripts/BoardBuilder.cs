﻿using UnityEngine;
using UnityEngine.UI;

public class BoardBuilder : MonoBehaviour
{
    [SerializeField] private GameObject cellPrefab;
    [SerializeField] private GameObject edgingPrefab;
    private Transform cellsParent;
    private int _width, _height;
    private GameObject[][] _cells;

    public GameObject[][] Cells
    {
        get { return _cells; }
    }

    void Start()
    {
        _width = 8;
        _height = 8;

        CreateCellsParent();
        _cells = GetBoardCells(_width, _height);
        CreateEdging(_cells);
        BoardManager.Instance.Cells = _cells;
    }

    /// <summary>
    /// Создаёт в сцене объект, к которому в последствии будут крепиться клетки шахматной доски.
    /// </summary>
    private void CreateCellsParent()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        cellsParent = new GameObject { name = "Cells", tag = "CellsParent" }.transform;
        cellsParent.SetParent(canvas.transform);
        cellsParent.localPosition = Vector3.zero;
    }

    /// <summary>
    /// Возвращает двумерный массив созданных клеток шахматной доски.
    /// </summary>
    /// <param name="width">Ширина шахматной доски.</param>
    /// <param name="height">Высота шахматной доски.</param>
    /// <returns></returns>
    private GameObject[][] GetBoardCells(int width, int height)
    {
        GameObject[][] cells = new GameObject[width][];
        for (int curWidth = 0; curWidth < width; curWidth++)
        {
            cells[curWidth] = new GameObject[height];
            for (int curHeight = 0; curHeight < height; curHeight++)
            {
                cells[curWidth][curHeight] = GetCell(curWidth, curHeight);
            }
        }

        return cells;
    }

    /// <summary>
    /// Возвращает созданную клетку шахматной доски.
    /// </summary>
    /// <param name="curWidth"></param>
    /// <param name="curHeight"></param>
    /// <returns></returns>
    private GameObject GetCell(int curWidth, int curHeight)
    {
        GameObject cell = Instantiate(cellPrefab, cellsParent);
        cell.transform.localPosition = GetCellCoordinates(curWidth, curHeight);

        Cell curCell = cell.GetComponent<Cell>();
        if (curCell)
        {
            curCell.IsWhite = GetBlackOrWhite(curWidth, curHeight);
        }

        curCell.HomeForPawnOfColor = GetWhoseHomeIsCell(curWidth, curHeight);

        return cell;
    }

    /// <summary>
    /// Возвращает координаты клетки шахматной доски.
    /// </summary>
    /// <param name="curWidth"></param>
    /// <param name="curHeight"></param>
    /// <returns></returns>
    private Vector3 GetCellCoordinates(int curWidth, int curHeight)
    {
        // 100 - ширина и высота клетки шахматной доски в пикселях.
        // Можно приделать метод, рассчитывающий эти ширину и высоту, исходя из разрешения окна.
        int imageSize = 100;

        Vector3 coordinates = new Vector3();
        coordinates.x = curWidth * imageSize - 350;
        coordinates.y = curHeight * imageSize - 350;
        coordinates.z = 0f;

        return coordinates;
    }

    /// <summary>
    /// Возвращает какого цвета должна быть клетка.
    /// </summary>
    /// <param name="curWidth"></param>
    /// <param name="curHeight"></param>
    /// <returns>Возвращает true, если клетка белая. В другом случае возвращает false.</returns>
    private bool GetBlackOrWhite(int curWidth, int curHeight)
    {
        return ((curWidth + curHeight) % 2) != 0;
    }

    /// <summary>
    /// Получить чьим домом является клетка шахматной доски.
    /// </summary>
    /// <param name="curWidth"></param>
    /// <param name="curHeight"></param>
    /// <returns>Возвращает HomeForPawnOfColor.Black, если клетка является домом для чёрных пешек,
    /// HomeForPawnOfColor.White, если клетка является домом для белых пешек,
    /// HomeForPawnOfColor.IsNotHome, если клетка не является ни чьим домом.</returns>
    private HomeForPawnOfColor GetWhoseHomeIsCell(int curWidth, int curHeight)
    {
        if ((curWidth < 3) && (curHeight + 3 >= _width))
        {
            return HomeForPawnOfColor.Black;
        }

        if ((curHeight < 3) && (curWidth + 3 >= _width))
        {
            return HomeForPawnOfColor.White;
        }

        return HomeForPawnOfColor.IsNotHome;
    }

    private void CreateEdging(GameObject[][] cells)
    {
        Transform edgingParent = GetEdgingParent();

        int max_x = cells.Length - 1;
        int max_y = max_x;

        for (int x = 0; x <= max_x; x++)
        {
            for (int y = 0; y <= max_y; y++)
            {
                if (x == 0 || x == max_x)
                {
                    GameObject edgCell = Instantiate(edgingPrefab);
                    edgCell.transform.SetParent(edgingParent);

                    edgCell.GetComponent<Text>().text = (y + 1).ToString();
                    edgCell.transform.localPosition = new Vector3(cells[x][y].transform.localPosition.x + (x == 0 ? -100 : 100),
                        cells[x][y].transform.localPosition.y, 0);
                }

                if (y == 0 || y == max_y)
                {
                    GameObject edgCell = Instantiate(edgingPrefab);
                    edgCell.transform.SetParent(edgingParent);

                    edgCell.GetComponent<Text>().text = ((alphabet)x).ToString();
                    edgCell.transform.localPosition = new Vector3(cells[x][y].transform.localPosition.x,
                        cells[x][y].transform.localPosition.y + (y == 0 ? -100 : 100), 0);
                }
            }
        }
    }

    private Transform GetEdgingParent()
    {
        GameObject canvas = GameObject.FindGameObjectWithTag("Canvas");
        Transform edgingParent = new GameObject { name = "Edging", tag = "EdgingParent" }.transform;
        edgingParent.SetParent(canvas.transform);
        edgingParent.localPosition = Vector3.zero;

        return edgingParent;
    }

    private enum alphabet { a,b,c,d,e,f,g,h }
}
